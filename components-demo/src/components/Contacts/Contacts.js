import React, {Fragment}  from 'react';
import {Link} from 'react-router-dom';
import './Contacts.css';
import Header from "../Header/Header";

const Contacts= props => {
    return (
        <Fragment>
            <Header/>
          <div className='Border'>

            <h3>About Us</h3>
            <div>
              <p>Number: <strong>+(996)556-300-153</strong></p>
              <p>Secondary number: <strong>+(996)554-300-153</strong></p>
              <p>Email: <strong>sobolev@gmail.com</strong></p>
              <p>Secondary Email: <strong>Maysner@gmail.com</strong></p>
            </div>
            <div>
              <p>Location: <strong>Bishkek, st.Ahumbaeva 20</strong></p>
            </div>
            <div className='Back'>
              <Link to="/" className='Back-About'>Back</Link>
            </div>
          </div>
        </Fragment>
    )
};

export default Contacts;