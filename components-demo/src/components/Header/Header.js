import React from 'react';
import {Link} from "react-router-dom";
import './Header.css';


const Header = (props) => {
    return(
        <header className="Header">
            <div className="container clearfix">
                <div className="Logo">
                    <Link to="/">My Blog</Link>
                </div>
                <nav className="Nav">
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/add'>Add</Link></li>
                        <li><Link to='/about'>About</Link></li>
                        <li><Link to='/contacts'>Contacts</Link></li>
                    </ul>
                </nav>
            </div>

        </header>
    )
};

export default Header;