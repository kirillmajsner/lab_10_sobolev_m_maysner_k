import React, {Component} from 'react';
import axios from '../../axios-shop';
import Header from "../Header/Header";

import './Add.css';

class Add extends Component {

    state = {
        title: '',
        description: '',

    };

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value});
    };

    submitHandler = event => {
        event.preventDefault();
        console.log(this.state);
    };



    addPost = () => {
        const post = {title: this.state.title, description: this.state.description};
        axios.post('/posts.json', post).then((response) => {
            console.log(response);
            this.props.history.replace('/')
        })
    };
    render() {

        return (
            <div className="Add">
                <Header/>
                <div className="container">
                    <h2 className="title">Add New Post</h2>
                    <form className="Form" onSubmit={this.submitHandler}>
                        <input type="text" name="title" value={this.state.title} onChange={this.valueChanged}/>
                        <textarea name="description" cols="30" rows="10" value={this.state.description} onChange={this.valueChanged} />
                        <button className="Btn" onClick={this.addPost}>Save</button>
                    </form>
                </div>

            </div>
        );
    }
}

export default Add;