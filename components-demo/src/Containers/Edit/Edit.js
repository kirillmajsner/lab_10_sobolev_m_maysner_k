import React, {Component} from 'react';
import axios from "../../axios-shop";

class Edit extends Component {
  state = {
    title: '',
    description: '',

  };

  valueChanged = event => {
    const {name, value} = event.target;

    this.setState({[name]: value});
  };

  submitHandler = event => {
    event.preventDefault();
    console.log(this.state);
  };
  addPost = () => {
    const id = this.props.match.params.id;
    const post = {title: this.state.title, description: this.state.description};
    axios.put(`posts/${id}.json`, post).then((response) => {
      console.log(response);
      this.props.history.replace('/')
    })
  };
  render() {
    return (
      <div>
        <div className="container">
          <h2>Edit post</h2>
          <form className="Form" onSubmit={this.submitHandler}>
            <input type="text" name="title" value={this.state.title} onChange={this.valueChanged}/>
            <textarea name="description" cols="30" rows="10" value={this.state.description} onChange={this.valueChanged} />
            <button onClick={this.addPost}>Save</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Edit;