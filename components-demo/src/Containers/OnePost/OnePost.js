import React, {Component} from 'react';
import axios from '../../axios-shop'
import './OnePost.css';
import {NavLink} from "react-router-dom";

class OnePost extends Component {
  state={
    post: []
  };
  deleteHendler = () =>{
    const id = this.props.match.params.id;
    axios.delete(`posts/${id}.json`).then(() => {
      this.props.history.push('/');
    })
  };
  componentDidMount(){
    const id = this.props.match.params.id;
    axios.get(`posts/${id}.json`).then(response => {
      this.setState({post: response.data});

    })

  }
  render() {
    return (
      <div className='container'>
        <h3>{this.state.post.title}</h3>
        <p>{this.state.post.description}</p>
        <button onClick={this.deleteHendler}>Delete</button>
        <button><NavLink to={`/edit/${this.props.match.params.id}`}>change</NavLink></button>
      </div>
    );
  }
}

export default OnePost;