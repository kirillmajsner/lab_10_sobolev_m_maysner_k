import React, {Component, Fragment} from 'react';
import './Home.css';
import Header from "../../components/Header/Header";
import axios from "../../axios-shop";

class Home extends Component {


    state = {
        posts: null
    };

    componentDidMount() {
        axios.get('/posts.json')
            .then((response)=>{
              if(response.data !== null) {
                const posts = Object.keys(response.data).map(id =>{
                  return {...response.data[id], id}
                });
                this.setState({posts})
              }
            });
      console.log(this.state.posts);
    }
    readMore = (id) =>{
      this.props.history.push(`/posts/${id}`);
    };
    render() {
        let posts = null;
        if (this.state.posts){
          posts = this.state.posts.map((post, index)=>{
            return(
              <div key={index} className='Form-Dask'>
                <h5>{post.title}</h5>
                <button onClick={() => this.readMore(post.id)}>ReadMore >>></button>
              </div>
              )
          })
        }
        return (
            <Fragment>
                <Header/>
                <div className="container">
                  <div className='Dask'>
                    {posts}
                  </div>
                </div>


            </Fragment>
        );
    }
}

export default Home;