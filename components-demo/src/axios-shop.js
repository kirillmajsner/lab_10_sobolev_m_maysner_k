import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-blog-lab.firebaseio.com/'
});

export default instance