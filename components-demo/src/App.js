import React, { Component } from 'react';
import {Route, Switch, BrowserRouter} from "react-router-dom";
import Home from './Containers/Home/Home';
import About from './components/About/About';
import Contacts from './components/Contacts/Contacts';
import Add from './components/Add/Add';
import './App.css';
import OnePost from "./Containers/OnePost/OnePost";
import Edit from "./Containers/Edit/Edit";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path='/' exact component={Home}/>
            <Route path='/add' component={Add} />
            <Route path='/about' component={About}/>
            <Route path='/contacts' component={Contacts}/>
            <Route path='/posts/:id' component={OnePost}/>
            <Route path='/edit/:id' component={Edit}/>
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
